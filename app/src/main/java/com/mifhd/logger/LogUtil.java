package com.mifhd.logger;

import android.util.Log;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LogUtil {

    private static final int STACK_TRACE_LEVELS_UP = 5;

    public static void v(String msg) {
        if (BuildConfig.DEBUG) {
            Log.v("V>>:", ".(" + getClassName() + ":" +
                    getLineNumber() + ")->" +
                    getMethodName() + "():" +
                    msg);
        }
    }

    public static void d(final String msg) {
        if (BuildConfig.DEBUG) {
            Log.d("D>>:", ".(" + getClassName() + ":" +
                    getLineNumber() + ")->" +
                    getMethodName() + "():" +
                    msg);
        }
    }

    public static void i(final String msg) {
        if (BuildConfig.DEBUG) {
            Log.i("I>>:", ".(" + getClassName() + ":" +
                    getLineNumber() + ")->" +
                    getMethodName() + "():" +
                    msg);
        }
    }

    public static void w(final String msg) {
        if (BuildConfig.DEBUG) {
            Log.w("W>>:", ".(" + getClassName() + ":" +
                    getLineNumber() + ")->" +
                    getMethodName() + "():" +
                    msg);
        }
    }

    public static void e(final String msg) {
        if (BuildConfig.DEBUG) {
            Log.e("E>>:", ".(" + getClassName() + ":" +
                    getLineNumber() + ")->" +
                    getMethodName() + "():" +
                    msg);
        }
    }

    public static void wtf(final String msg) {
        if (BuildConfig.DEBUG) {
            Log.wtf("EWTF>>:", ".(" + getClassName() + ":" +
                    getLineNumber() + ")->" +
                    getMethodName() + "():" +
                    msg);
        }
    }

    public static void t(final Throwable tr) {
        if (BuildConfig.DEBUG) {
            Log.e("EWTF>>:", ".(" + getClassName() + ":" +
                    getLineNumber() + ")->" +
                    getMethodName() + "():" +
                    tr);
        }
    }


    private static int getLineNumber() {
        return Thread.currentThread().getStackTrace()[STACK_TRACE_LEVELS_UP].getLineNumber();
    }

    private static String getClassName() {
        String fileName = Thread.currentThread().getStackTrace()[STACK_TRACE_LEVELS_UP].getFileName();

        // Removing ".java" and returning class name
        // return fileName.substring(0, fileName.length() - 5);
        return fileName;
    }

    private static String getMethodName() {
        return Thread.currentThread().getStackTrace()[STACK_TRACE_LEVELS_UP].getMethodName();
    }

    private static String getClassNameMethodNameAndLineNumber() {
        return "[" + getClassName() + ":" + getLineNumber() + "->" + getMethodName() + "()]: ";
    }
}