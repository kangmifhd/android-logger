package com.mifhd.logger;

import android.util.Log;

public class Logger {

    private static final String SEPARATOR = ", ";

    public static void v(final String...msg) {
        if (BuildConfig.DEBUG) {
            final StackTraceElement stackTrace = new Exception().getStackTrace()[1];
            String fileName = stackTrace.getFileName();
            if (fileName == null)
                fileName = "";
            final String info = ".(" + fileName + ":"
                    + stackTrace.getLineNumber() + ")->" + stackTrace.getMethodName() + "()";
            Log.v("V>>", info + ": " + getMessage(msg));
        }
    }

    public static void d(final String...msg) {
        if (BuildConfig.DEBUG) {
            final StackTraceElement stackTrace = new Exception().getStackTrace()[1];
            String fileName = stackTrace.getFileName();
            if (fileName == null)
                fileName = "";
            final String info = ".(" + fileName + ":"
                    + stackTrace.getLineNumber() + ")->" + stackTrace.getMethodName() + "()";
            Log.d("D>>", info + ": " + getMessage(msg));
        }
    }

    public static void i(final String...msg) {
        if (BuildConfig.DEBUG) {
            final StackTraceElement stackTrace = new Exception().getStackTrace()[1];
            String fileName = stackTrace.getFileName();
            if (fileName == null)
                fileName = "";
            final String info = ".(" + fileName + ":"
                    + stackTrace.getLineNumber() + ")->" + stackTrace.getMethodName() + "()";
            Log.i("I>>", info + ": " + getMessage(msg));
        }
    }

    public static void w(final String...msg) {
        if (BuildConfig.DEBUG) {
            final StackTraceElement stackTrace = new Exception().getStackTrace()[1];
            String fileName = stackTrace.getFileName();
            if (fileName == null)
                fileName = "";
            final String info = ".(" + fileName + ":"
                    + stackTrace.getLineNumber() + ")->" + stackTrace.getMethodName() + "()";
            Log.w("W>>", info + ": " + getMessage(msg));
        }
    }

    public static void wtf(final String...msg) {
        if (BuildConfig.DEBUG) {
            final StackTraceElement stackTrace = new Exception().getStackTrace()[1];
            String fileName = stackTrace.getFileName();
            if (fileName == null)
                fileName = "";
            final String info = ".(" + fileName + ":"
                    + stackTrace.getLineNumber() + ")->" + stackTrace.getMethodName() + "()";
            Log.wtf("WTF>>", info + ": " + getMessage(msg));
        }
    }

    public static void t(Throwable tr) {
        if (BuildConfig.DEBUG) {
            final StackTraceElement stackTrace = new Exception().getStackTrace()[1];
            String fileName = stackTrace.getFileName();
            if (fileName == null)
                fileName = "";
            final String info = ".(" + fileName + ":"
                    + stackTrace.getLineNumber() + ")->" + stackTrace.getMethodName() + "()";
            Log.e("Throwable>>", info, tr);
        }
    }

    public static void e(final String...msg) {
        if (BuildConfig.DEBUG) {
            final StackTraceElement stackTrace = new Exception().getStackTrace()[1];
            String fileName = stackTrace.getFileName();
            if (fileName == null)
                fileName = "";
            final String info = ".(" + fileName + ":"
                    + stackTrace.getLineNumber() + ")->" + stackTrace.getMethodName() + "()";
            Log.e("E>>", info + ": " + getMessage(msg));
        }
    }

    private static String getMessage(String[] message)
    {
        if (BuildConfig.DEBUG)
        {
            String str = "";
            for (String m: message){
                str += m + SEPARATOR;
            }
            return str.replaceAll(SEPARATOR +"$", "");
        }
        return null;
    }

}